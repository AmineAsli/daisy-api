from django.db import models

class Customer(models.Model):
    name = models.CharField(max_length=250)
    company_name = models.CharField(max_length=250)
    active = models.BooleanField(default=True)
    phone = models.CharField(max_length=1024)
    email = models.CharField(max_length=250)
    address1 = models.CharField(max_length=1024)
    address2 = models.CharField(max_length=1024)
    address3 = models.CharField(max_length=1024)
    address4 = models.CharField(max_length=1024)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str(self):
        return self.name

class Note(models.Model):
    customer = models.OneToOneField(
        Customer,
        on_delete=models.CASCADE,
        null=True,
    )
    body = models.TextField()

class BillingInformation(models.Model):
    customer = models.OneToOneField(
        Customer,
        on_delete=models.CASCADE,
        null=True,
    )
    discount = models.DecimalField(max_digits=2, decimal_places=2, blank=True, null=True)
    credit_limit = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True)