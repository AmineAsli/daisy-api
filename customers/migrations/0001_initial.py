# Generated by Django 3.0.1 on 2019-12-24 18:06

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Customer',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=250)),
                ('company_name', models.CharField(max_length=250)),
                ('active', models.BooleanField(default=True)),
                ('phone', models.CharField(max_length=1024)),
                ('email', models.CharField(max_length=250)),
                ('address1', models.CharField(max_length=1024)),
                ('address2', models.CharField(max_length=1024)),
                ('address3', models.CharField(max_length=1024)),
                ('address4', models.CharField(max_length=1024)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
        ),
    ]
