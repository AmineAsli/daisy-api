from django.test import TestCase

from .models import Customer, Note, BillingInformation

class CustomerModelTest(TestCase):

    def setUp(self):

        self.customer1 = Customer.objects.create(name='Jane Doe', company_name='abc', active=True)
        self.customer2 = Customer.objects.create(name='John Doe', company_name='xyz', active=True)
        
        self.note1 = Note.objects.create(customer=self.customer1, body='Good customer')
        self.note2 = Note.objects.create(customer=self.customer2, body='Loyal customer')

        self.billing_information1 = BillingInformation.objects.create(customer=self.customer1, discount=0.33, credit_limit=1000.9)
        self.billing_information2 = BillingInformation.objects.create(customer=self.customer2, discount=0.25, credit_limit=99.99)
        
    def test_name(self):
        expected_customer1_name = f'{self.customer1.name}'
        expected_customer2_name = f'{self.customer2.name}'

        self.assertEquals(expected_customer1_name, 'Jane Doe')
        self.assertEquals(expected_customer2_name, 'John Doe')
    
    def test_company_name(self):
        expected_customer1_company_name = f'{self.customer1.company_name}'
        expected_customer2_company_name = f'{self.customer2.company_name}'

        self.assertEquals(expected_customer1_company_name, 'abc')
        self.assertEquals(expected_customer2_company_name, 'xyz')
    
    def test_note_body(self):
        expected_note1 = f'{self.note1.body}'
        expected_note2 = f'{self.note2.body}'

        self.assertEquals(expected_note1, 'Good customer')
        self.assertEquals(expected_note2, 'Loyal customer')

    def test_customer_note_body(self):
        expected_customer1_note1 = f'{self.customer1.note.body}'
        expected_customer2_note2 = f'{self.customer2.note.body}'

        self.assertEquals(expected_customer1_note1, 'Good customer')
        self.assertEquals(expected_customer2_note2, 'Loyal customer')
    
    def test_billing_information_discount(self):
        expected_billing_information1_discount = self.billing_information1.discount
        expected_billing_information2_discount = self.billing_information2.discount

        self.assertEquals(expected_billing_information1_discount, 0.33)
        self.assertNotEquals(expected_billing_information1_discount, 0.329)
        self.assertEquals(expected_billing_information2_discount, 0.250)
        self.assertNotEquals(expected_billing_information2_discount, 0.251)
    
    def test_billing_information_credit_limit(self):
        expected_billing_information1_credit_limit = self.billing_information1.credit_limit
        expected_billing_information2_credit_limit = self.billing_information2.credit_limit

        self.assertEquals(expected_billing_information1_credit_limit, 1000.9)
        self.assertNotEquals(expected_billing_information1_credit_limit, 1000.899)
        self.assertEquals(expected_billing_information2_credit_limit, 99.990)
        self.assertNotEquals(expected_billing_information2_credit_limit, 99.989)